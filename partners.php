<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Global It  Management</title>
   <link rel="icon" href="images/logo.png" type="image/png" sizes="16x16">
  <link rel="stylesheet" href="css/style.css">

  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link href="css/bootstrap-4-navbar.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>

<body>
  <!-- Static navbar Start-->
 <?php 
			require('header.php');
			?>
  <!-- SLIDER END  -->


  <section class="Members">
    <div class="container">
      <h1 class="text-center">Partners</h1>
    

<?php

 $qry = "select * from partner";
       $res = mysql_query($qry);
    $i=0;
    
   ?>
  <div class="Paragraph-Bg">
        <div class="row text-dark">
    
<?php
while($row=mysql_fetch_array($res))
  { 
    if($i%3==0){?>

      </div>
    </div>
    <div class="Paragraph-Bg">
        <div class="row text-dark">


 <?php   } ?>

        <div class="col-md-4">
            <div class="card" style="width: 18rem;background-color:rgb(0,0,0,0.6); color:#fff;">
              <img class="card-img-top" src="admin/uploads/partner/<?php echo $row['logo'];  ?>" alt="partner">
              <div class="card-body">
                <h5 class="card-title"><?php echo $row['name'];  ?></h5>
                <p class="card-text"><?php echo $row['description'];?></p>
                <a href="<?php echo $row['website'];?>" class="btn btn-primary">Visit Web</a>
              </div>
            </div>
          </div>

    <?php 

$i++;
  } ?>
    

        </div>
      </div>
    </div>
  </section>



  <!-- footer start -->
  <?php 
			require('footer.php');
			?>
  <!-- footer end -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

  <script src="js/bootstrap-4-navbar.js"></script>
</body>

</html>
