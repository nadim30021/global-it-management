<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Global It  Management</title>
   <link rel="icon" href="images/logo.png" type="image/png" sizes="16x16">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link href="css/bootstrap-4-navbar.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <!-- Static navbar Start-->
 <?php 
			require('header.php');
			?>
  <!-- SLIDER END  -->



<!-- members section start -->
<section class="Members">
  <div class="container">
    <h1 class="text-center">Awards</h1> <br> <br>



<?php

 $qry = "select * from awards";
       $res = mysql_query($qry);
    $i=0;
    
   ?>
<div class="row">
    
<?php
while($row=mysql_fetch_array($res))
  { 
    if($i%3==0){?>

        </div>
      <br>
    <div class="row">
    


 <?php   }

    ?>
    <div class="col-md-4">
        <div class="card"  style="background-color:rgba(0, 0, 0, 0.6);color: #fff; ">
        <div class="card-body">
         
          <div style="display: inline-block;" align="center">
            <h4> <?php echo $row['event_name']; ?> &nbsp;</h4>
          <h5 class="card-title">Position : <?php echo $row['position']; ?></h5>
          <p class="card-text">Player : <?php echo $row['player']; ?></p>
        </div>
        </div>
      </div>
     </div>
    <?php 

$i++;
  } ?>
    
      </div>
   

<br>





  </div>
</section>
<!-- members section ends -->



  <!-- footer start -->
 <?php 
			require('footer.php');
			?>
  <!-- footer end -->






  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  <script src="js/bootstrap-4-navbar.js"></script>
</body>
</html>
