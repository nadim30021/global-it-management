<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Global It  Management</title>
  <link rel="icon" href="images/logo.png" type="image/png" sizes="16x16">
  <link rel="stylesheet" href="css/style.css">

  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link href="css/bootstrap-4-navbar.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>

<body>
  <!-- Static navbar Start-->
   <?php 
			require('header.php');
			?>
  <!-- SLIDER END  -->


<!-- latest News Section start  -->
<section class="News">
  <div class="container">
    <div class="Head">
      <h1 class="text-center Headline"> Latest News </h1>
    </div>

    <br> <br>
    

<?php

 $qry = "select * from latestnews";
       $res = mysql_query($qry);
    $i=0;
    
   ?>
<div class="row">
    
<?php
while($row=mysql_fetch_array($res))
  { 
    if($i%3==0){?>

        </div>
      <br>
    <div class="row">
    


 <?php   }

    ?>
    <div class="col-md-4">
        <div >
        <div class="card-body">
         
          <div style="display: inline-block;" align="center">
           <a href="<?php echo $row['link'];?>"><img class= "img-fluid img-thumbnail mt-2" src="admin/uploads/latest_news/<?php echo $row['newsImage'];?>" alt="news"></a>  
        </div>
        </div>
      </div>
     </div>
    <?php 

$i++;
  } ?>
    
      </div>
   

<br>















                
   
   

<br>



     </div>
  </div>
</section>
<!-- latest News Section end  -->

  <!-- Upcoming Tournaments starts-->
  <section class="Upcoming">
    <div class="container">
      <h1 class="text-center Headline"> Upcoming Tournaments </h1>
      <br> <br>
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <div class="row">
            <div class="col">
              <div class="accordion" id="accordionExample">


<?php
 $qry = "select * from tournament order by date desc";
       $res = mysql_query($qry);
    $i=1;
    $numbers = array("One", "Two", "Three", "Four" , "Five" , "Six", "Seven", "Eight", "Nine" , "Ten");
while($row=mysql_fetch_array($res))
  { ?>

            <div class="card">
                  <div class="card-header " id="heading<?php echo $numbers[$i-1];?>">
                    <h5 class="mb-0">
            <button class="btn btn-link collapsed text-light" type="button" data-toggle="collapse" data-target="#collapse<?php echo $numbers[$i-1];?>" aria-expanded="false" aria-controls="collapse<?php echo $numbers[$i-1];?>">
              Tournament <?php echo $i; ?>
               </button>
                 </h5>
                  </div>
                  <div id="collapse<?php echo $numbers[$i-1];?>" class="collapse" aria-labelledby="heading<?php echo $numbers[$i-1];?>" data-parent="#accordionExample">
                    <div class="card-body">
                      <h5><?php echo $row['title']; ?></h5>
                      <p class="text-justify"><?php echo $row['description']; ?></p>
                    </div>
                  </div>
                </div>




<?php  $i++; }

?>



              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Upcoming Tournaments ends  -->


<!-- footer start -->
 <?php 
			require('footer.php');
			?>
<!-- footer end -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

  <script src="js/bootstrap-4-navbar.js"></script>
</body>

</html>
