<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Global It  Management</title>
   <link rel="icon" href="images/logo.png" type="image/png" sizes="16x16">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link href="css/bootstrap-4-navbar.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <!-- Static navbar Start-->
  <?php 
			require('header.php');
			?>
  <!-- SLIDER END  -->



<!-- Career section start -->
<section class="Members">
    <div class="container">
      <h1 class="text-center">Rules & Regulation</h1>
    <div class="Paragraph-Bg">
      <p><?php $res=mysql_query("select * from game_registration_setting where id='4'");
$row=mysql_fetch_array($res); 
echo $row['rules_regulation'];
$s = $row['registration_open'];
?></p>
    

<br>
    



<div class="column mcb-column one column_fancy_heading "><div class="fancy_heading fancy_heading_icon"><h2 class="title" style="color:red;">Registration Fee and Procedure</h2></div>
</div>

<div class="column mcb-column one column_image "><div class="image_frame image_item no_link scale-with-grid no_border hover-disable"><div class="image_wrapper"><img class="scale-with-grid" src="images/rocket.png" alt="DotA2" width="100%" height="30%"></div></div>
</div>

<br>
<div class="column_attr clearfix align_justify" style=""><h5><ol>
<li style="text-align: justify;color:white;">Dial *322# from your personal Rocket account and Select 'Payment'</li>
<li style="text-align: justify;color:white;">At the next menu, you have to select 'Merchant Payment'</li>
<li style="text-align: justify;color:white;">At step number three you have to type our 12 Digit Rocket Merchant Account number '017715231125'</li>
<li style="text-align: justify;color:white;">Type 'Dota2' in the reference section</li>
<li style="text-align: justify;color:white;">Enter '1500' in amount section. BDT 1500 is the registration fee, you do not need to pay any extra charges other than the given amount.</li>
<li style="text-align: justify;color:white;">Enter Your Account PIN number and press submit or send in your menu</li>
<li style="text-align: justify;color:white;">After you are done with the whole process you will get the menu given in 7th Step of the upper image</li>
<li style="text-align: justify;color:white;">You will get a return Message where you will get the Transaction ID (Txnid). Make sure to note the Txnid because you will need that during and after registration</li>
<li style="text-align: justify;color:white;">Now if you are done with the payment, click the button below and Proceed to Registration</li>
</ol>
<br>
<p><span style="color: #ff0000;">Note</span></p>
<ul>
<li style="text-align: justify;color:white;">We will take maximum 24 hours to verify your registration. After the verification, we will upload your team's roster in our Participants section. So we request you to have patience.</li>
<li style="text-align: justify;color:white;">In case you do not have a personal Rocket number, you can visit Nearest Agent or DBBL Fast Track.</li>
<li style="text-align: justify;color:white;">For further assistant, call us on this number '09678800405'</li>
</ul></h5></div>

<div align="center">
<?php if($s=='1'){ ?>
<a href="register.php?4" type="button" name="button" class="btn btn-primary">Register Now</a>
<?php }
else if($s=='0'){ ?>
<a href="#" type="button" name="button" class="btn btn-primary">Registration Closed</a>
<?php }
?>
</div>

</div>



    </div>
</section>
<!-- career section ends -->



  <!-- footer start -->
  <?php 
			require('footer.php');
			?>
  <!-- footer end -->






  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  <script src="js/bootstrap-4-navbar.js"></script>
</body>
</html>
