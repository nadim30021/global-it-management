<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Global It  Management</title>
   <link rel="icon" href="images/logo.png" type="image/png" sizes="16x16">
  <link rel="stylesheet" href="css/style.css">

  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link href="css/bootstrap-4-navbar.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>




<body>
  <!-- Static navbar Start-->
  <?php 
			require('header.php');
			?>
  <!-- SLIDER END  -->



<?php
if(isset($_REQUEST['submit'])){

      $errors= array();
      $gameid=$_REQUEST['gameid'];
      $tname=$_REQUEST['tname'];
      $ttag=$_REQUEST['ttag'];
      $cname=$_REQUEST['cname'];
      $cmnmbr=$_REQUEST['cmnmbr'];
      $cmail=$_REQUEST['cmail'];
      $csteam=$_REQUEST['csteam'];
      $p2mail=$_REQUEST['p2mail'];
      $p2steam=$_REQUEST['p2steam'];
      $p3mail=$_REQUEST['p3mail'];
      $p3steam=$_REQUEST['p3steam'];
      $p4mail=$_REQUEST['p4mail'];
      $p4steam=$_REQUEST['p4steam'];
      $p5mail=$_REQUEST['p5mail'];
      $p5steam=$_REQUEST['p5steam'];
      $sb1mail=$_REQUEST['sb1mail'];
      $sb1steam=$_REQUEST['sb1steam'];
      $sb2mail=$_REQUEST['sb2mail'];
      $sb2steam=$_REQUEST['sb2steam'];
      $bkash=$_REQUEST['bkash'];
      $trid=$_REQUEST['trid'];


      $file_name = $_FILES['image']['name'];
      $file_size =$_FILES['image']['size'];
      $file_tmp =$_FILES['image']['tmp_name'];
      $file_type=$_FILES['image']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
      
      $expensions= array("jpeg","jpg","png");
      
      if(in_array($file_ext,$expensions)=== false){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
      
      if($file_size > 2097152){
         $errors[]='File size must be excately 2 MB';
      }
      


      if(empty($errors)==true){
        
         move_uploaded_file($file_tmp,"uploads/team_logo/".$file_name);
              
         date_default_timezone_set("Asia/Dhaka");
         $datee = date("Y-m-d G:i:s");

         $insert=mysql_query("INSERT INTO team_reg_information(gameid,team_name,team_tag,team_logo,captin_name,captin_mobile,captain_email,captin_steam,captin_img,captin_game_name,player2_name,player2_email,player2_steam,player2_img,player2_game_name,player3_name,player3_email,player3_steam,player3_img,player3_game_name,player4_name,player4_email,player4_steam,player4_img,player4_game_name,player5_name,player5_email,player5_steam,player5_img,player5_game_name,sub1_name,sub1_email,sub1_steam,sub1_img,sub1_game_name,sub2_name,sub2_email,sub2_steam,sub2_img,sub2_game_name,bkash,trax_id,reg_date,status) VALUES('$gameid','$tname','$ttag','$file_name','$cname','$cmnmbr','$cmail','$csteam','','','','$p2mail','$p2steam','','','','$p3mail','$p3steam','','','','$p4mail','$p4steam','','','','$p5mail','$p5steam','','','','$sb1mail','$sb1steam','','','','$sb2mail','$sb2steam','','','$bkash','$trid','$datee','1')");
         if($insert)
         {
          header("location:register.php?succ");
          echo "<script>window.location='register.php?succ';</script>";
         }

      }else{
         print_r($errors);
      }
   }


?>







<br> <br>
<section class="Sign-Up">
  <br>
<div class="container" style="background-color: white">
  <h2 class="text-center">Registration Form</h2><br>




 <?php
               if(isset($_REQUEST['succ']))
               {
              ?> 
              
               <div class="alert alert-block alert-error">
                

               <i class="icon-trash green"></i>
                <strong class="green" style="color:green">
                  Team Registration Complete !!!
                </strong>
            
              </div>
               
               <?php }
               
               ?>








  <form class="register-form" method="post"  enctype="multipart/form-data" action="">
    <input type="hidden" name="gameid" value="<?php echo $_REQUEST['id'];?>">
    <div class="form-group">
      <div class="row">
        <div class="col-sm-4">
          <label for="teamname">Team Name :</label>
          <input type="text" name="tname" id="tname" placeholder="Team Name" class="form-control" required="required">
        </div>
        <div class="col-sm-4">
          <label for="ttag">Team Tag :</label>
          <input type="text" name="ttag" id="ttag" placeholder="Team Tag" class="form-control" required="required">
        </div>
        <div class="col-sm-4">
          <label for="tlogo">Team Logo :</label>
          <input type="file" name="image" id="image" required="required">
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <div class="col-sm-6">
          <label for="cname">Captain's Name :</label>
          <input type="text" name="cname" id="cname" placeholder="Captain's Name" class="form-control" required="required">
        </div>
        <div class="col-sm-6">
          <label for="cmnmbr">Captain's Mobile Number :</label>
          <input type="text" name="cmnmbr" id="cmnmbr" placeholder="Captain's Mobile Number" class="form-control" required="required">
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <div class="col-sm-6">
          <label for="cmail">Captain's Email Id :</label>
          <input type="email" name="cmail" id="cmail" placeholder="Captain's Email Id" class="form-control" required="required">
        </div>
        <div class="col-sm-6">
          <label for="csteam">Captain's Steam Id :</label>
          <input type="text" name="csteam" id="csteam" placeholder="Captain's Steam Id" class="form-control" required="required">
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <div class="col-sm-6">
          <label for="p2mail">Player-2 Email Id :</label>
          <input type="email" name="p2mail" id="p2mail" placeholder="Player-2 Email Id" class="form-control" required="required">
        </div>
        <div class="col-sm-6">
          <label for="p2steam">Player-2 Steam Id :</label>
          <input type="text" name="p2steam" id="p2steam" placeholder="Player-2 Steam Id" class="form-control" required="required">
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <div class="col-sm-6">
          <label for="p3mail">Player-3 Email Id :</label>
          <input type="email" name="p3mail" id="p3mail" placeholder="Player-3 Email Id" class="form-control" required="required">
        </div>
        <div class="col-sm-6">
          <label for="p3steam">Player-3 Steam Id :</label>
          <input type="text" name="p3steam" id="p3steam" placeholder="Player-3 Steam Id" class="form-control" required="required">
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <div class="col-sm-6">
          <label for="p4mail">Player-4 Email Id :</label>
          <input type="email" name="p4mail" id="p4mail" placeholder="Player-4 Email Id" class="form-control" required="required">
        </div>
        <div class="col-sm-6">
          <label for="p4steam">Player-4 Steam Id :</label>
          <input type="text" name="p4steam" id="p4steam" placeholder="Player-4 Steam Id" class="form-control" required="required">
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <div class="col-sm-6">
          <label for="p5mail">Player-5 Email Id :</label>
          <input type="email" name="p5mail" id="p5mail" placeholder="Player-5 Email Id" class="form-control" required="required">
        </div>
        <div class="col-sm-6">
          <label for="p5steam">Player-5 Steam Id :</label>
          <input type="text" name="p5steam" id="p5steam" placeholder="Player-5 Steam Id" class="form-control" required="required">
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <div class="col-sm-6">
          <label for="sb1mail">SubPlayer-1 Email Id :</label>
          <input type="email" name="sb1mail" id="sb1mail" placeholder="Sub Player-1 Email Id" class="form-control" >
        </div>  
        <div class="col-sm-6">
          <label for="sb1steam">SubPlayer-1 Steam Id :</label>
          <input type="text" name="sb1steam" id="sb1steam" placeholder="Sub Player-1 Steam Id" class="form-control" >
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <div class="col-sm-6">
          <label for="sb2mail">Sub Player-2 Email Id :</label>
          <input type="email" name="sb2mail" id="sb2mail" placeholder="Sub Player-2 Email Id" class="form-control" >
        </div>  
        <div class="col-sm-6">
          <label for="sb2steam">Sub Player-2 Steam Id :</label>
          <input type="text" name="sb2steam" id="sb2steam" placeholder="Sub Player-2 Steam Id" class="form-control" >
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <div class="col-sm-6">
          <label for="bkash">Bkash Number :</label>
          <input type="text" name="bkash" id="bkash" class="form-control" placeholder="Bkash Number" required="required">
        </div>  
        <div class="col-sm-6">
          <label for="transaction">Transaction Id :</label>
          <input type="text" name="trid" id="trid" class="form-control" placeholder="Transaction Id" required="required">
        </div>
      </div>
    </div>
    <br>
    <input class="btn btn-success btn-block" type="submit" name="submit" value="Submit"><br><br>
  </form>
</div>

</section>

<br> <br>

<!-- footer start -->
<?php 
			require('footer.php');
			?>
<!-- footer end -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

  <script src="js/bootstrap-4-navbar.js"></script>
</body>

</html>
