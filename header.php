<?php
include("admin/config/error.php");
include("admin/includes/function.php");
?>

<!-- Static navbar Start-->
<section class="Navbar fixed-top">
  <nav class="navbar navbar-expand-md navbar-light ">
      <a class="navbar-brand ml-5 text-white" href="#"><img class="img-fluid" src="images/logo.png" alt="logo" style="height: 60px; width:200px;"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav mx-auto">
              <li class="nav-item active">
                  <a class="nav-link text-white" href="index.php">HOME</a>
              </li>

              <li class="nav-item dropdown ">
                  <a class="nav-link dropdown-toggle text-white"  id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    eSports
                  </a>
                  <ul class="dropdown-menu HoverItems" aria-labelledby="navbarDropdownMenuLink">

                      <li><a class="dropdown-item dropdown-toggle ">TEAM</a>
                          <ul class="dropdown-menu">
                              
                              <?php

       $qry = "select * from game";
       $res = mysql_query($qry);
       $i=0;
                  while($row=mysql_fetch_array($res))
                  {  $gameId=$row['id']; ?>
                             
                              <li><a class="dropdown-item dropdown-toggle"><img class="img-fluid" src="admin/uploads/game/<?php echo $row['icon']?>" alt="game1" style="width: 25px; height: 25px;"></a>
                                  <?php
                      $qry2 = "select * from team where gameId='$gameId'";
                      $res2 = mysql_query($qry2);
                      $numr = mysql_num_rows($res2);
                      if($numr>0){ ?>
                                  <ul class="dropdown-menu">
                                      <?php  while($row2=mysql_fetch_array($res2))
                                 { ?>
                                      <li><a class="dropdown-item" href="team_member.php?id=<?php echo $row2['id'] ?>"><?php echo $row2['teamName']; ?></a></li>
                                <?php } 
                                  ?>     
                                  </ul>
                                  <?php }
                                ?>
                              </li>
                    <?php  }

                        ?>


<!--
                              <li><a class="dropdown-item" href="#">GAMING HOUSE</a></li>
                              <li><a class="dropdown-item" href="#">PARTICIPANTS</a></li> -->
                          </ul>
                      </li>
                     <!-- <li><a class="dropdown-item" href="gaminghouse.php">GAMING HOUSE</a></li> -->
                      <li><a class="dropdown-item" href="participants_game_list.php">	PARTICIPANTS  </a></li>
                      <li><a class="dropdown-item" href="live_streaming.php"> LIVE STREAMING  </a></li>
                  </ul>
              </li>


              <li class="nav-item dropdown Dropdown2">
                  <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    MORE
                  </a>
                  <ul class="dropdown-menu HoverItems2" aria-labelledby="navbarDropdownMenuLink">
                      <li><a class="dropdown-item" href="career.php">CAREER</a></li>
                      <li><a class="dropdown-item" href="awards.php">AWARDS</a></li>
                      <li><a class="dropdown-item" href="partners.php">PARTNERS</a></li>
                      <li><a class="dropdown-item" href="register_game_list.php">REGISTRATION  </a></li>
                  </ul>
              </li>
              <li class="nav-item">
                  <a class="nav-link text-white" href="events.php">EVENTS</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link text-white" href="about.php">ABOUT</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link text-white" href="contact.php">CONTACT</a>
              </li>
          </ul>
      </div>


      <a class="" href="#"> <span> <i class="fab fa-facebook text-white"></i> </span> </a>
      <a class="ml-2" href="#"> <span> <i class="fab fa-twitter text-white"></i> </span> </a>
  </nav>
</section>
<!-- Static navbar Ends-->



<!-- SLIDER START  -->
<div id="carouselId" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselId" data-slide-to="0" class="active"></li>
    <li data-target="#carouselId" data-slide-to="1"></li>
    <li data-target="#carouselId" data-slide-to="2"></li>

  </ol>
  <div class="carousel-inner" role="listbox">

    <?php

       $qry = "select * from slider";
       $res = mysql_query($qry);
       $i=0;
  while($row=mysql_fetch_array($res))
  {

    if($i==0){ ?>

    <div class="carousel-item active">

  <?php $i++; }
  else {?>

  <div class="carousel-item">
  <?php } ?>

    <img class="img-fluid" src="images/<?php echo $row['slider_url'];?>" alt="First slide">
      <div class="carousel-caption  ">
        <div class="row">
          <div class="col">
            <h1 class="text-light font-weight-bold"><?php echo $row['slider_title']; ?></h1>
            <a href="register_game_list.php" type="button" name="button" class="btn btn-primary">Register Now</a>
          </div>
        </div>
      </div>
    </div>



    <?php } ?>

  </div>
  <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
  <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
</div>
<!-- SLIDER END  -->
