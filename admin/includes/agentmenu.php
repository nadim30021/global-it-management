<?php $currentpagename=getPageName(); ?>

<div class="col-sm-3">

	<h4 class="navbar-inner text-center" style="color:#FFF; line-height:40px; background-color: #0F9B16;">Profile menu</h4>

	<div class="service-box2">

		<ul class="proleftmenu prfl_lft_menu">
			
			
			
			<li <?php if($currentpagename=="admin_dashboard.php") { echo 'class="current"'; } ?>>
				<a href="admin_dashboard.php" class="leftmenua">
					Dashboard
				</a>
			</li>

			<li <?php if($currentpagename=="slider_manage.php") { echo 'class="current"'; } ?>>
				<a href="slider_manage.php" class="leftmenua">
					Slider Manage
				</a>
			</li>



			<li <?php if($currentpagename=="latest_news_manage.php") { echo 'class="current"'; } ?>>
				<a href="latest_news_manage.php" class="leftmenua">
					Latest News Manage
				</a>
			</li>

			<li <?php if($currentpagename=="upcomming_tournaments.php") { echo 'class="current"'; } ?>>
				<a href="upcomming_tournaments.php" class="leftmenua">
					Upcomming Tournaments
				</a>
			</li>

			<li <?php if($currentpagename=="upcomming_events.php") { echo 'class="current"'; } ?>>
				<a href="upcomming_events.php" class="leftmenua">
					Events Manage
				</a>
			</li>
			<li <?php if($currentpagename=="career.php") { echo 'class="current"'; } ?>>
				<a href="career.php" class="leftmenua">
					Career Update
				</a>
			</li>
			<li <?php if($currentpagename=="about.php") { echo 'class="current"'; } ?>>
				<a href="about.php" class="leftmenua">
					About Update
				</a>
			</li>
			<li <?php if($currentpagename=="awards.php") { echo 'class="current"'; } ?>>
				<a href="awards.php" class="leftmenua">
					Awards Add
				</a>
			</li>

			<li <?php if($currentpagename=="game_add.php") { echo 'class="current"'; } ?>>
				<a href="game_add.php" class="leftmenua">
					Game Add
				</a>
			</li>

			<li <?php if($currentpagename=="team_add.php") { echo 'class="current"'; } ?>>
				<a href="team_add.php" class="leftmenua">
					Team Add
				</a>
			</li>


			<li <?php if($currentpagename=="management_member_add.php") { echo 'class="current"'; } ?>>
				<a href="management_member_add.php" class="leftmenua">
					Management Member
				</a>
			</li>

			<li <?php if($currentpagename=="partner_add.php") { echo 'class="current"'; } ?>>
				<a href="partner_add.php" class="leftmenua">
					Partner
				</a>
			</li>

			<li <?php if($currentpagename=="rules_regulation.php") { echo 'class="current"'; } ?>>
				<a href="rules_regulation.php" class="leftmenua">
					Rules & Regulation
				</a>
			</li>


			
			<li <?php if($currentpagename=="registered_game_list.php") { echo 'class="current"'; } ?>>
				<a href="registered_game_list.php" class="leftmenua">
					Registered Team
				</a>
			</li>

			<li <?php if($currentpagename=="registered_game_list.php") { echo 'class="current"'; } ?>>
				<a href="videos.php" class="leftmenua">
					Videos
				</a>
			</li>


			<li <?php if($currentpagename=="admin_pass_change.php") { echo 'class="current"'; } ?>>
				<a href="admin_pass_change.php" class="leftmenua">
					Password Change
				</a>
			</li>
			<li <?php if($currentpagename=="logout.php") { echo 'class="current"'; } ?>>
				<a href="logout.php" class="leftmenua">
					Logout
				</a>
			</li>
			
		</ul>

	</div>

</div>