-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2018 at 08:36 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `globalit`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `agent_profileid` varchar(255) NOT NULL,
  `agent_name` varchar(255) NOT NULL,
  `agent_email` varchar(255) NOT NULL,
  `agent_phone` varchar(255) NOT NULL,
  `agent_password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `agent_profileid`, `agent_name`, `agent_email`, `agent_phone`, `agent_password`) VALUES
(4, 'admin', 'Global IT Management', 'globalitmanagement@gmail.com', '01715069189', '123456a');

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

CREATE TABLE `awards` (
  `id` int(11) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `player` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `awards`
--

INSERT INTO `awards` (`id`, `event_name`, `position`, `player`) VALUES
(1, 'abcd', '1st', 'hasfhghgas, swjghhagjds, asjdghadj, asdghgasdh'),
(2, 'sgdgf', '3rd', 'hasfhghgas, swjghhagjds, asjdghadj, asdghgasdh'),
(3, 'werwe', '4th', 'hasfhghgas, swjghhagjds, asjdghadj, asdghgasdh'),
(4, 'qwe', '5th', 'hasfhghgas, swjghhagjds, asjdghadj, asdghgasdh');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `title`, `description`, `status`, `date`) VALUES
(1, '', '', '0', '2018-09-17 08:43:20'),
(2, 'fgh', 'gfh', 'finished', '2018-09-27 00:23:29'),
(3, ' Season 29: Sep 4, 2018 - Nov 12, 2018', ' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam dolore minima repudiandae, quia unde necessitatibus libero, obcaecati deleniti natus quos.', 'upcomming', '2018-09-28 21:13:41');

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`id`, `name`, `icon`) VALUES
(2, 'CS GO', 'game1.png'),
(3, 'Call of duty', 'game5.png');

-- --------------------------------------------------------

--
-- Table structure for table `game_registration_setting`
--

CREATE TABLE `game_registration_setting` (
  `id` int(11) NOT NULL,
  `game_name` varchar(255) NOT NULL,
  `rules_regulation` longtext NOT NULL,
  `registration_open` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game_registration_setting`
--

INSERT INTO `game_registration_setting` (`id`, `game_name`, `rules_regulation`, `registration_open`) VALUES
(1, 'Dota 2', '<div class="column_attr clearfix align_justify">\n<h5 style="text-align: justify; color: white;">Before joining the tournament, please read the following rules and guidelines. Applying or joining the tournament means you agree and will follow the rules we set.</h5>\n<br />\n<h4 style="color: red;">For Your Information</h4>\n<ul>\n<li style="text-align: justify; color: white;">Any team can register</li>\n<li style="text-align: justify; color: white;">Last Date of Registration is 20th July 2018</li>\n<li style="text-align: justify; color: white;">Registration fee is one time nonrefundable unless the tournament faces cancellation due to unavoidable circumstances</li>\n</ul>\n<br />\n<h4 style="color: red;">Tournament Rules</h4>\n<ul>\n<li style="text-align: justify; color: white;">Only 8 teams can register and it will be first come first serve basis</li>\n<li style="text-align: justify; color: white;">Tournament will start from 26th September 2018</li>\n<li style="text-align: justify; color: white;">Matches will be played in Online Lobby and the Server Location will be Singapore</li>\n<li style="text-align: justify; color: white;">Format of the tournament will be Double elimination. All the matches will be played Best of 1. Only Grand Final will be played Best of 5 (There will be one game advantage towards the upper bracket finalist)</li>\n<li style="text-align: justify; color: white;">The Bracket and Schedule of the main event will be published on 20th August 2018</li>\n<li style="text-align: justify; color: white;">No request will be considered regarding the schedule</li>\n<li style="text-align: justify; color: white;">First Match of the Day will start at 10:00am sharp. Failure to reach in time will lead to Penalties and forfeit.</li>\n<li style="text-align: justify; color: white;">Teams must report 15 min prior their match time. They will be given 10 minutes to prepare their platform. After that, match will start.</li>\n<li style="text-align: justify; color: white;">Time schedule will be strictly followed but exceptions might be there. (e.g. power failure, technical issues etc.)</li>\n</ul>\n<br />\n<h4 style="color: red;">Penalties</h4>\n<ul>\n<li style="text-align: justify; color: white;">Level 1 = -20 Bonus Time</li>\n<li style="text-align: justify; color: white;">Level 2 = -50 Bonus Time</li>\n<li style="text-align: justify; color: white;">Level 3 = -90 Bonus Time</li>\n</ul>\n<br />\n<h4 style="color: red;">General Rules</h4>\n<ul>\n<li style="text-align: justify; color: white;">Insults and unfair or disrespectful behavior towards anyone will not be tolerated and will be punished accordingly</li>\n<li style="text-align: justify; color: white;">If any serious bugs occur, the game has to be paused immediately and the organizers decides how to continue</li>\n<li style="text-align: justify; color: white;">Participants are requested to bring their own gaming gears (e.g. mouse, mouse pad, keyboard, joystick)</li>\n<li style="text-align: justify; color: white;">You can bring your monitor at your own risk. Organizers are not liable if any kind of damage occurs (Don''t forget to bring the cables)</li>\n<li style="text-align: justify; color: white;">By attending the competition participants acknowledge without limitation to comply with the rules and regulations and with the decisions made by the organizers</li>\n<li style="text-align: justify; color: white;">Every participant acknowledges the right of the Organizers to modify the rules and regulations for adjustments at any time without notice</li>\n<li style="text-align: justify; color: white;">Every team accepts the official schedule of the competition and declares its ability to be available during these times</li>\n</ul>\n<br />\n<h4 style="color: red;">Important Notes</h4>\n<ul>\n<li style="text-align: justify; color: white;">We urge all the participants to cooperate so that we all together can host a fair competition among all. Our best efforts will be there to make it happen with your all in all support</li>\n<li style="text-align: justify; color: white;">If you cannot cooperate with the delays or technical issues (If occurs) then we request you not to register</li>\n<li style="text-align: justify; color: white;">All 8 teams (every players) who played in the tournament, must be present at 8th September to recieve the prizes</li>\n</ul>\n</div>', 0),
(2, 'Rainbow Six', '<div class="column_attr clearfix align_justify">\n<h4 style="text-align: justify; color: white;">Before joining the tournament, please read the following rules and guidelines. Applying or joining the tournament means you agree and will follow the rules we set.</h4>\n<br />\n<h4 style="color: red;">For Your Information</h4>\n<ul>\n<li style="text-align: justify; color: white;">Any team can register</li>\n<li style="text-align: justify; color: white;">Last Date of Registration is 20th July 2018</li>\n<li style="text-align: justify; color: white;">Registration fee is one time nonrefundable unless the tournament faces cancellation due to unavoidable circumstances</li>\n</ul>\n<br />\n<h4 style="color: red;">Tournament Rules</h4>\n<ul>\n<li style="text-align: justify; color: white;">Only 8 teams can register and it will be first come first serve basis</li>\n<li style="text-align: justify; color: white;">Tournament will start from 26th September 2018</li>\n<li style="text-align: justify; color: white;">Matches will be played in Online Lobby and the Server Location will be Singapore</li>\n<li style="text-align: justify; color: white;">Format of the tournament will be Double elimination. All the matches will be played Best of 1. Only Grand Final will be played Best of 5 (There will be one game advantage towards the upper bracket finalist)</li>\n<li style="text-align: justify; color: white;">The Bracket and Schedule of the main event will be published on 20th August 2018</li>\n<li style="text-align: justify; color: white;">No request will be considered regarding the schedule</li>\n<li style="text-align: justify; color: white;">First Match of the Day will start at 10:00am sharp. Failure to reach in time will lead to Penalties and forfeit.</li>\n<li style="text-align: justify; color: white;">Teams must report 15 min prior their match time. They will be given 10 minutes to prepare their platform. After that, match will start.</li>\n<li style="text-align: justify; color: white;">Time schedule will be strictly followed but exceptions might be there. (e.g. power failure, technical issues etc.)</li>\n</ul>\n<br />\n<h4 style="color: red;">Penalties</h4>\n<ul>\n<li style="text-align: justify; color: white;">Level 1 = -20 Bonus Time</li>\n<li style="text-align: justify; color: white;">Level 2 = -50 Bonus Time</li>\n<li style="text-align: justify; color: white;">Level 3 = -90 Bonus Time</li>\n</ul>\n<br />\n<h4 style="color: red;">General Rules</h4>\n<ul>\n<li style="text-align: justify; color: white;">Insults and unfair or disrespectful behavior towards anyone will not be tolerated and will be punished accordingly</li>\n<li style="text-align: justify; color: white;">If any serious bugs occur, the game has to be paused immediately and the organizers decides how to continue</li>\n<li style="text-align: justify; color: white;">Participants are requested to bring their own gaming gears (e.g. mouse, mouse pad, keyboard, joystick)</li>\n<li style="text-align: justify; color: white;">You can bring your monitor at your own risk. Organizers are not liable if any kind of damage occurs (Don''t forget to bring the cables)</li>\n<li style="text-align: justify; color: white;">By attending the competition participants acknowledge without limitation to comply with the rules and regulations and with the decisions made by the organizers</li>\n<li style="text-align: justify; color: white;">Every participant acknowledges the right of the Organizers to modify the rules and regulations for adjustments at any time without notice</li>\n<li style="text-align: justify; color: white;">Every team accepts the official schedule of the competition and declares its ability to be available during these times</li>\n</ul>\n<br />\n<h4 style="color: red;">Important Notes</h4>\n<ul>\n<li style="text-align: justify; color: white;">We urge all the participants to cooperate so that we all together can host a fair competition among all. Our best efforts will be there to make it happen with your all in all support</li>\n<li style="text-align: justify; color: white;">If you cannot cooperate with the delays or technical issues (If occurs) then we request you not to register</li>\n<li style="text-align: justify; color: white;">All 8 teams (every players) who played in the tournament, must be present at 8th September to recieve the prizes</li>\n</ul>\n</div>', 1),
(3, 'League of Legends', '<div class="column_attr clearfix align_justify">\r\n<h4 style="text-align: justify; color: white;">Before joining the tournament, please read the following rules and guidelines. Applying or joining the tournament means you agree and will follow the rules we set.</h4>\r\n<br />\r\n<h4 style="color: red;">For Your Information</h4>\r\n<ul>\r\n<li style="text-align: justify; color: white;">Any team can register</li>\r\n<li style="text-align: justify; color: white;">Last Date of Registration is 20th July 2018</li>\r\n<li style="text-align: justify; color: white;">Registration fee is one time nonrefundable unless the tournament faces cancellation due to unavoidable circumstances</li>\r\n</ul>\r\n<br />\r\n<h4 style="color: red;">Tournament Rules</h4>\r\n<ul>\r\n<li style="text-align: justify; color: white;">Only 8 teams can register and it will be first come first serve basis</li>\r\n<li style="text-align: justify; color: white;">Tournament will start from 26th September 2018</li>\r\n<li style="text-align: justify; color: white;">Matches will be played in Online Lobby and the Server Location will be Singapore</li>\r\n<li style="text-align: justify; color: white;">Format of the tournament will be Double elimination. All the matches will be played Best of 1. Only Grand Final will be played Best of 5 (There will be one game advantage towards the upper bracket finalist)</li>\r\n<li style="text-align: justify; color: white;">The Bracket and Schedule of the main event will be published on 20th August 2018</li>\r\n<li style="text-align: justify; color: white;">No request will be considered regarding the schedule</li>\r\n<li style="text-align: justify; color: white;">First Match of the Day will start at 10:00am sharp. Failure to reach in time will lead to Penalties and forfeit.</li>\r\n<li style="text-align: justify; color: white;">Teams must report 15 min prior their match time. They will be given 10 minutes to prepare their platform. After that, match will start.</li>\r\n<li style="text-align: justify; color: white;">Time schedule will be strictly followed but exceptions might be there. (e.g. power failure, technical issues etc.)</li>\r\n</ul>\r\n<br />\r\n<h4 style="color: red;">Penalties</h4>\r\n<ul>\r\n<li style="text-align: justify; color: white;">Level 1 = -20 Bonus Time</li>\r\n<li style="text-align: justify; color: white;">Level 2 = -50 Bonus Time</li>\r\n<li style="text-align: justify; color: white;">Level 3 = -90 Bonus Time</li>\r\n</ul>\r\n<br />\r\n<h4 style="color: red;">General Rules</h4>\r\n<ul>\r\n<li style="text-align: justify; color: white;">Insults and unfair or disrespectful behavior towards anyone will not be tolerated and will be punished accordingly</li>\r\n<li style="text-align: justify; color: white;">If any serious bugs occur, the game has to be paused immediately and the organizers decides how to continue</li>\r\n<li style="text-align: justify; color: white;">Participants are requested to bring their own gaming gears (e.g. mouse, mouse pad, keyboard, joystick)</li>\r\n<li style="text-align: justify; color: white;">You can bring your monitor at your own risk. Organizers are not liable if any kind of damage occurs (Don''t forget to bring the cables)</li>\r\n<li style="text-align: justify; color: white;">By attending the competition participants acknowledge without limitation to comply with the rules and regulations and with the decisions made by the organizers</li>\r\n<li style="text-align: justify; color: white;">Every participant acknowledges the right of the Organizers to modify the rules and regulations for adjustments at any time without notice</li>\r\n<li style="text-align: justify; color: white;">Every team accepts the official schedule of the competition and declares its ability to be available during these times</li>\r\n</ul>\r\n<br />\r\n<h4 style="color: red;">Important Notes</h4>\r\n<ul>\r\n<li style="text-align: justify; color: white;">We urge all the participants to cooperate so that we all together can host a fair competition among all. Our best efforts will be there to make it happen with your all in all support</li>\r\n<li style="text-align: justify; color: white;">If you cannot cooperate with the delays or technical issues (If occurs) then we request you not to register</li>\r\n<li style="text-align: justify; color: white;">All 8 teams (every players) who played in the tournament, must be present at 8th September to recieve the prizes</li>\r\n</ul>\r\n</div>', 0),
(4, 'CSGO', '<div class="column_attr clearfix align_justify">\n<h4 style="text-align: justify; color: white;">Before joining the tournament, please read the following rules and guidelines. Applying or joining the tournament means you agree and will follow the rules we set.</h4>\n<br />\n<h4 style="color: red;">For Your Information</h4>\n<ul>\n<li style="text-align: justify; color: white;">Any team can register</li>\n<li style="text-align: justify; color: white;">Last Date of Registration is 20th July 2018</li>\n<li style="text-align: justify; color: white;">Registration fee is one time nonrefundable unless the tournament faces cancellation due to unavoidable circumstances</li>\n</ul>\n<br />\n<h4 style="color: red;">Tournament Rules</h4>\n<ul>\n<li style="text-align: justify; color: white;">Only 8 teams can register and it will be first come first serve basis</li>\n<li style="text-align: justify; color: white;">Tournament will start from 26th September 2018</li>\n<li style="text-align: justify; color: white;">Matches will be played in Online Lobby and the Server Location will be Singapore</li>\n<li style="text-align: justify; color: white;">Format of the tournament will be Double elimination. All the matches will be played Best of 1. Only Grand Final will be played Best of 5 (There will be one game advantage towards the upper bracket finalist)</li>\n<li style="text-align: justify; color: white;">The Bracket and Schedule of the main event will be published on 20th August 2018</li>\n<li style="text-align: justify; color: white;">No request will be considered regarding the schedule</li>\n<li style="text-align: justify; color: white;">First Match of the Day will start at 10:00am sharp. Failure to reach in time will lead to Penalties and forfeit.</li>\n<li style="text-align: justify; color: white;">Teams must report 15 min prior their match time. They will be given 10 minutes to prepare their platform. After that, match will start.</li>\n<li style="text-align: justify; color: white;">Time schedule will be strictly followed but exceptions might be there. (e.g. power failure, technical issues etc.)</li>\n</ul>\n<br />\n<h4 style="color: red;">Penalties</h4>\n<ul>\n<li style="text-align: justify; color: white;">Level 1 = -20 Bonus Time</li>\n<li style="text-align: justify; color: white;">Level 2 = -50 Bonus Time</li>\n<li style="text-align: justify; color: white;">Level 3 = -90 Bonus Time</li>\n</ul>\n<br />\n<h4 style="color: red;">General Rules</h4>\n<ul>\n<li style="text-align: justify; color: white;">Insults and unfair or disrespectful behavior towards anyone will not be tolerated and will be punished accordingly</li>\n<li style="text-align: justify; color: white;">If any serious bugs occur, the game has to be paused immediately and the organizers decides how to continue</li>\n<li style="text-align: justify; color: white;">Participants are requested to bring their own gaming gears (e.g. mouse, mouse pad, keyboard, joystick)</li>\n<li style="text-align: justify; color: white;">You can bring your monitor at your own risk. Organizers are not liable if any kind of damage occurs (Don''t forget to bring the cables)</li>\n<li style="text-align: justify; color: white;">By attending the competition participants acknowledge without limitation to comply with the rules and regulations and with the decisions made by the organizers</li>\n<li style="text-align: justify; color: white;">Every participant acknowledges the right of the Organizers to modify the rules and regulations for adjustments at any time without notice</li>\n<li style="text-align: justify; color: white;">Every team accepts the official schedule of the competition and declares its ability to be available during these times</li>\n</ul>\n<br />\n<h4 style="color: red;">Important Notes</h4>\n<ul>\n<li style="text-align: justify; color: white;">We urge all the participants to cooperate so that we all together can host a fair competition among all. Our best efforts will be there to make it happen with your all in all support</li>\n<li style="text-align: justify; color: white;">If you cannot cooperate with the delays or technical issues (If occurs) then we request you not to register</li>\n<li style="text-align: justify; color: white;">All 8 teams (every players) who played in the tournament, must be present at 8th September to recieve the prizes</li>\n</ul>\n</div>', 0),
(5, 'FIFA 18', '<div class="column_attr clearfix align_justify">\n<h4 style="text-align: justify; color: white;">Before joining the tournament, please read the following rules and guidelines. Applying or joining the tournament means you agree and will follow the rules we set.</h4>\n<br />\n<h4 style="color: red;">For Your Information</h4>\n<ul>\n<li style="text-align: justify; color: white;">Any team can register</li>\n<li style="text-align: justify; color: white;">Last Date of Registration is 20th July 2018</li>\n<li style="text-align: justify; color: white;">Registration fee is one time nonrefundable unless the tournament faces cancellation due to unavoidable circumstances</li>\n</ul>\n<br />\n<h4 style="color: red;">Tournament Rules</h4>\n<ul>\n<li style="text-align: justify; color: white;">Only 8 teams can register and it will be first come first serve basis</li>\n<li style="text-align: justify; color: white;">Tournament will start from 26th September 2018</li>\n<li style="text-align: justify; color: white;">Matches will be played in Online Lobby and the Server Location will be Singapore</li>\n<li style="text-align: justify; color: white;">Format of the tournament will be Double elimination. All the matches will be played Best of 1. Only Grand Final will be played Best of 5 (There will be one game advantage towards the upper bracket finalist)</li>\n<li style="text-align: justify; color: white;">The Bracket and Schedule of the main event will be published on 20th August 2018</li>\n<li style="text-align: justify; color: white;">No request will be considered regarding the schedule</li>\n<li style="text-align: justify; color: white;">First Match of the Day will start at 10:00am sharp. Failure to reach in time will lead to Penalties and forfeit.</li>\n<li style="text-align: justify; color: white;">Teams must report 15 min prior their match time. They will be given 10 minutes to prepare their platform. After that, match will start.</li>\n<li style="text-align: justify; color: white;">Time schedule will be strictly followed but exceptions might be there. (e.g. power failure, technical issues etc.)</li>\n</ul>\n<br />\n<h4 style="color: red;">Penalties</h4>\n<ul>\n<li style="text-align: justify; color: white;">Level 1 = -20 Bonus Time</li>\n<li style="text-align: justify; color: white;">Level 2 = -50 Bonus Time</li>\n<li style="text-align: justify; color: white;">Level 3 = -90 Bonus Time</li>\n</ul>\n<br />\n<h4 style="color: red;">General Rules</h4>\n<ul>\n<li style="text-align: justify; color: white;">Insults and unfair or disrespectful behavior towards anyone will not be tolerated and will be punished accordingly</li>\n<li style="text-align: justify; color: white;">If any serious bugs occur, the game has to be paused immediately and the organizers decides how to continue</li>\n<li style="text-align: justify; color: white;">Participants are requested to bring their own gaming gears (e.g. mouse, mouse pad, keyboard, joystick)</li>\n<li style="text-align: justify; color: white;">You can bring your monitor at your own risk. Organizers are not liable if any kind of damage occurs (Don''t forget to bring the cables)</li>\n<li style="text-align: justify; color: white;">By attending the competition participants acknowledge without limitation to comply with the rules and regulations and with the decisions made by the organizers</li>\n<li style="text-align: justify; color: white;">Every participant acknowledges the right of the Organizers to modify the rules and regulations for adjustments at any time without notice</li>\n<li style="text-align: justify; color: white;">Every team accepts the official schedule of the competition and declares its ability to be available during these times</li>\n</ul>\n<br />\n<h4 style="color: red;">Important Notes</h4>\n<ul>\n<li style="text-align: justify; color: white;">We urge all the participants to cooperate so that we all together can host a fair competition among all. Our best efforts will be there to make it happen with your all in all support</li>\n<li style="text-align: justify; color: white;">If you cannot cooperate with the delays or technical issues (If occurs) then we request you not to register</li>\n<li style="text-align: justify; color: white;">All 8 teams (every players) who played in the tournament, must be present at 8th September to recieve the prizes</li>\n</ul>\n</div>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `latestnews`
--

CREATE TABLE `latestnews` (
  `id` int(11) NOT NULL,
  `newsImage` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `latestnews`
--

INSERT INTO `latestnews` (`id`, `newsImage`, `link`) VALUES
(2, 'news2.png', 'ghj'),
(3, 'news3.png', 'sdf'),
(4, 'news4.png', 'gfh');

-- --------------------------------------------------------

--
-- Table structure for table `management`
--

CREATE TABLE `management` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `team` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `management`
--

INSERT INTO `management` (`id`, `name`, `designation`, `image`, `team`, `facebook`, `twitter`) VALUES
(4, 'nadim', 'CEO', 'DSC_0054.JPG.png', 'abcd', 'http://facebook.com/nadimul2', 'http://twitter.com/nadim321');

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`id`, `name`, `logo`, `description`, `website`) VALUES
(1, 'Partner1', 'logo1.png', 'Some quick example text to build on the card title and make up the bulk of the card''s content.', 'www.test.com'),
(2, 'Partner2', 'sponsor500x500_star.png', 'Some quick example text to build on the card title and make up the bulk of the card''s content.', 'www.test2.com'),
(3, 'rty', 'card1.jpg', 'Some quick example text to build on the card title and make up the bulk of the card''s content.', 'tryrt');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `career` longtext NOT NULL,
  `about` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `career`, `about`) VALUES
(1, '<p>Global It Management is a multigaming organisation established in 2006. We support national and international teams all around the world, currently active in countries like Germany, Czech Republic, Austria, Hungary, Pakistan, Colombia &amp; North America.</p>\r\n<p>Additionally to that we have players from United Kingdom, Canada, Romania, Albania and many more countries representing myRevenge e.V.</p>\r\n<p>Even our staff is located all around the world. And this could be your opportunity to gain a foothold in eSports, even though you may have little to no experience by now. Because we believe that everyone needs to have a chance to prove himself!</p>\r\n<p>&nbsp;</p>\r\n<p>Current job vacancies:</p>\r\n<p>Team Manager &ndash; requirements:</p>\r\n<ul>\r\n<li>Be willing to learn about the teams'' needs and our work flow</li>\r\n<li>Interact active and on a daily basis with the teams and staff</li>\r\n<li>Age 18+</li>\r\n<li>No bans on any gaming accounts (for example Steam VAC ban, ESL 12pps, FaceIt, etc.)</li>\r\n<li>Participate in our weekly management meetings</li>\r\n</ul>\r\n<p>Team Manager &ndash; tasks:</p>\r\n<ul>\r\n<li>Taking care of and be informed about the assigned teams</li>\r\n<li>Supporting the teams in league and tournament related administration</li>\r\n<li>Looking for possible events to participate (online/offline)</li>\r\n<li>Making sure there is an active communication between the team and the staff</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>Social Media Manager &ndash; requirements:</p>\r\n<ul>\r\n<li>Be willing to learn how to handle social media accounts and content</li>\r\n<li>Be willing to interact active and on a daily basis with the staff and gather informations about our teams, sponsors, gaming house as content for our social media sites</li>\r\n<li>Age 18+</li>\r\n<li>No bans on any gaming accounts (for example Steam VAC ban, ESL 12pps, FaceIt, etc.)</li>\r\n<li>be fluent in Englisch (spoken and in written form)</li>\r\n</ul>\r\n<p>Social Media Manager &ndash; tasks:</p>\r\n<ul>\r\n<li>Posting related content on our Facebook sites and Twitter about the teams</li>\r\n<li>Posting related content on our Facebook sites and Twitter page about the sponsors</li>\r\n<li>Posting related content on our Facebook sites and Twitter page about our streamers going live</li>\r\n<li>Pushing our social media sites with daily related content to push likes, subscriber and follower</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Everyone has started somewhere as a streamer, that''s why beginners with a small follower base are very welcomed at myRevenge e.V.!</p>\r\n<p>&nbsp;</p>\r\n<p>Streamer &ndash; requirements:</p>\r\n<ul>\r\n<li>Stream on a regular and active basis</li>\r\n<li>Age related to the streamed content/game</li>\r\n<li>No bans on any gaming accounts (for example Steam VAC ban, ESL 12pps, FaceIt, etc.)</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>If you are interested in any vacancy listed above and think you would be a great fit for our organisation write us via email at: jobs@myrevenge.net</p>\r\n<p>&nbsp;</p>\r\n<p>* Every vacancy is on a voluntary basis!</p>', '<p>Global It Management is a multigaming organisation established in 2006. We support national and international teams all around the world, currently active in countries like Germany, Czech Republic, Austria, Hungary, Pakistan, Colombia &amp; North America.</p>\r\n<p>Additionally to that we have players from United Kingdom, Canada, Romania, Albania and many more countries representing myRevenge e.V.</p>\r\n<p>Even our staff is located all around the world. And this could be your opportunity to gain a foothold in eSports, even though you may have little to no experience by now. Because we believe that everyone needs to have a chance to prove himself!</p>\r\n<p>&nbsp;</p>\r\n<p>Current job vacancies:</p>\r\n<p>Team Manager &ndash; requirements:</p>\r\n<ul>\r\n<li>Be willing to learn about the teams'' needs and our work flow</li>\r\n<li>Interact active and on a daily basis with the teams and staff</li>\r\n<li>Age 18+</li>\r\n<li>No bans on any gaming accounts (for example Steam VAC ban, ESL 12pps, FaceIt, etc.)</li>\r\n<li>Participate in our weekly management meetings</li>\r\n</ul>\r\n<p>Team Manager &ndash; tasks:</p>\r\n<ul>\r\n<li>Taking care of and be informed about the assigned teams</li>\r\n<li>Supporting the teams in league and tournament related administration</li>\r\n<li>Looking for possible events to participate (online/offline)</li>\r\n<li>Making sure there is an active communication between the team and the staff</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>Social Media Manager &ndash; requirements:</p>\r\n<ul>\r\n<li>Be willing to learn how to handle social media accounts and content</li>\r\n<li>Be willing to interact active and on a daily basis with the staff and gather informations about our teams, sponsors, gaming house as content for our social media sites</li>\r\n<li>Age 18+</li>\r\n<li>No bans on any gaming accounts (for example Steam VAC ban, ESL 12pps, FaceIt, etc.)</li>\r\n<li>be fluent in Englisch (spoken and in written form)</li>\r\n</ul>\r\n<p>Social Media Manager &ndash; tasks:</p>\r\n<ul>\r\n<li>Posting related content on our Facebook sites and Twitter about the teams</li>\r\n<li>Posting related content on our Facebook sites and Twitter page about the sponsors</li>\r\n<li>Posting related content on our Facebook sites and Twitter page about our streamers going live</li>\r\n<li>Pushing our social media sites with daily related content to push likes, subscriber and follower</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Everyone has started somewhere as a streamer, that''s why beginners with a small follower base are very welcomed at myRevenge e.V.!</p>\r\n<p>&nbsp;</p>\r\n<p>Streamer &ndash; requirements:</p>\r\n<ul>\r\n<li>Stream on a regular and active basis</li>\r\n<li>Age related to the streamed content/game</li>\r\n<li>No bans on any gaming accounts (for example Steam VAC ban, ESL 12pps, FaceIt, etc.)</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>If you are interested in any vacancy listed above and think you would be a great fit for our organisation write us via email at: jobs@myrevenge.net</p>\r\n<p>&nbsp;</p>\r\n<p>* Every vacancy is on a voluntary basis!</p>');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `slider_url` varchar(255) NOT NULL,
  `slider_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `slider_url`, `slider_title`) VALUES
(4, 'header.jpg', 'Title1'),
(5, 'header2.jpg', 'Title2'),
(6, 'header3.jpg', 'Title3');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `gameId` int(11) NOT NULL,
  `teamName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `gameId`, `teamName`) VALUES
(4, 2, 'Dhaka1'),
(5, 2, 'Khulna1');

-- --------------------------------------------------------

--
-- Table structure for table `team_member`
--

CREATE TABLE `team_member` (
  `id` int(11) NOT NULL,
  `teamId` int(11) NOT NULL,
  `playerName` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_member`
--

INSERT INTO `team_member` (`id`, `teamId`, `playerName`, `facebook`, `twitter`, `image`) VALUES
(14, 3, 'asd', 'asd', 'wqew', 'DSC_0054.jpg'),
(15, 3, 'sadsdf', 'ds', 'fsdf', 'header.jpg'),
(16, 4, 'Nadim', 'http://facebook.com/nadimul2', 'nadim321', 'DSC_0054.png'),
(17, 4, 'Bapin', 'bapin2', 'bapin321', 'DSC_0054.jpg'),
(18, 5, 'Bapin', 'bapin321', 'bapin2', 'DSC_0054.jpg'),
(19, 5, 'Nadim', 'nadim321', 'nadim2', 'DSC_0054.png'),
(20, 4, 'Bapin', 'bapin3', 'sfsd', 'DSC_0054.jpg'),
(21, 4, 'Bapin', 'bapin3', 'sfsd', 'DSC_0054.jpg'),
(22, 4, 'Bapin', 'bapin3', 'sfsd', 'DSC_0054.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `team_reg_information`
--

CREATE TABLE `team_reg_information` (
  `id` int(11) NOT NULL,
  `gameid` int(11) NOT NULL,
  `team_name` varchar(255) NOT NULL,
  `team_tag` varchar(255) NOT NULL,
  `team_logo` varchar(255) NOT NULL,
  `captin_name` varchar(255) NOT NULL,
  `captin_mobile` varchar(20) NOT NULL,
  `captain_email` varchar(255) NOT NULL,
  `captin_steam` varchar(255) NOT NULL,
  `captin_img` varchar(255) NOT NULL,
  `captin_game_name` varchar(255) NOT NULL,
  `player2_name` varchar(255) NOT NULL,
  `player2_email` varchar(255) NOT NULL,
  `player2_steam` varchar(255) NOT NULL,
  `player2_img` varchar(255) NOT NULL,
  `player2_game_name` varchar(255) NOT NULL,
  `player3_name` varchar(255) NOT NULL,
  `player3_email` varchar(255) NOT NULL,
  `player3_steam` varchar(255) NOT NULL,
  `player3_img` varchar(255) NOT NULL,
  `player3_game_name` varchar(255) NOT NULL,
  `player4_name` varchar(255) NOT NULL,
  `player4_email` varchar(255) NOT NULL,
  `player4_steam` varchar(255) NOT NULL,
  `player4_img` varchar(255) NOT NULL,
  `player4_game_name` varchar(255) NOT NULL,
  `player5_name` varchar(255) NOT NULL,
  `player5_email` varchar(255) NOT NULL,
  `player5_steam` varchar(255) NOT NULL,
  `player5_img` varchar(255) NOT NULL,
  `player5_game_name` varchar(255) NOT NULL,
  `sub1_name` varchar(255) NOT NULL,
  `sub1_email` varchar(255) NOT NULL,
  `sub1_steam` varchar(255) NOT NULL,
  `sub1_img` varchar(255) NOT NULL,
  `sub1_game_name` varchar(255) NOT NULL,
  `sub2_name` varchar(255) NOT NULL,
  `sub2_email` varchar(255) NOT NULL,
  `sub2_steam` varchar(255) NOT NULL,
  `sub2_img` varchar(255) NOT NULL,
  `sub2_game_name` varchar(255) NOT NULL,
  `bkash` varchar(20) NOT NULL,
  `trax_id` varchar(255) NOT NULL,
  `reg_date` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_reg_information`
--

INSERT INTO `team_reg_information` (`id`, `gameid`, `team_name`, `team_tag`, `team_logo`, `captin_name`, `captin_mobile`, `captain_email`, `captin_steam`, `captin_img`, `captin_game_name`, `player2_name`, `player2_email`, `player2_steam`, `player2_img`, `player2_game_name`, `player3_name`, `player3_email`, `player3_steam`, `player3_img`, `player3_game_name`, `player4_name`, `player4_email`, `player4_steam`, `player4_img`, `player4_game_name`, `player5_name`, `player5_email`, `player5_steam`, `player5_img`, `player5_game_name`, `sub1_name`, `sub1_email`, `sub1_steam`, `sub1_img`, `sub1_game_name`, `sub2_name`, `sub2_email`, `sub2_steam`, `sub2_img`, `sub2_game_name`, `bkash`, `trax_id`, `reg_date`, `status`) VALUES
(1, 2, 'test', 'ert', 'card1.jpg', 'c', '104454', 'c@gmail.com', 'c', '76176e7f2f2a9458_256x256.png', 'cc', 'p2', 'p2@gmail.com', 'p2', '38260581_917366771807338_8580501267564986368_n.jpg', 'pp2', 'p3', 'p3@gmail.com', 'p3', 'DSC_0054.jpg', 'pp3', 'p4', 'p4@gmail.com', 'p4', 'DSC_0054.png', 'pp4', 'p5', 'p5@gmail.com', 'p5', 'header.jpg', 'pp5', 's1', 's1@gmail.com', 's1', 'header2.jpg', 'ss1', 's2', 's2@gmail.com', 's2', 'header3.jpg', 'ss3', '054845554', '5Jyyagywqwe', '2018-10-03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tournament`
--

CREATE TABLE `tournament` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tournament`
--

INSERT INTO `tournament` (`id`, `title`, `description`, `date`) VALUES
(2, 'Season 20: Sep 10, 2018 - Nov 12, 2018', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam dolore minima repudiandae, quia unde necessitatibus libero, obcaecati deleniti natus quos.', '2018-09-17 08:09:20'),
(3, 'Season 21: Sep 20, 2018 - Nov 20, 2018', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam dolore minima repudiandae, quia unde necessitatibus libero, obcaecati deleniti natus quos.', '2018-09-17 08:29:27'),
(4, 'Season 22: Oct 2, 2018 - Nov 25, 2018', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam dolore minima repudiandae, quia unde necessitatibus libero, obcaecati deleniti natus quos.', '2018-09-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `name`, `url`) VALUES
(2, 'sdasdsa', 'https://www.youtube.com/embed/JJpcCKKqc4k'),
(3, 'sdfsdf', 'https://www.youtube.com/embed/JJpcCKKqc4k');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_registration_setting`
--
ALTER TABLE `game_registration_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `latestnews`
--
ALTER TABLE `latestnews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `management`
--
ALTER TABLE `management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_member`
--
ALTER TABLE `team_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_reg_information`
--
ALTER TABLE `team_reg_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tournament`
--
ALTER TABLE `tournament`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `awards`
--
ALTER TABLE `awards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `game_registration_setting`
--
ALTER TABLE `game_registration_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `latestnews`
--
ALTER TABLE `latestnews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `management`
--
ALTER TABLE `management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `team_member`
--
ALTER TABLE `team_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `team_reg_information`
--
ALTER TABLE `team_reg_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tournament`
--
ALTER TABLE `tournament`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
