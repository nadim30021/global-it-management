<?php
class common {
	
	public function insertrec($query){
		if(!mysql_query("$query")){
			$err1 = mysql_errno();
			$err2 = mysql_error();
			echo "$err1 $err2";
			exit;			
		}
		return;
	}
	
	public function singlerec($query){
		if (! ($result = mysql_query ("$query"))) {
			$err1 = mysql_errno();
			$err2 = mysql_error();
			echo ("$query  $err1 $err2");
			exit;
		}
		$rw = mysql_fetch_array ($result);
		mysql_free_result ($result);
		return $rw;
	}

	public function get_all($query){
		$rst = mysql_query("$query");
		$result = array();
		$x=0;
		while($row=mysql_fetch_array($rst)){
			$result[$x] = $row;
			$x++;
		}				
		return $result;
	}
	
	public function FetchAll($query){
		$rst = mysql_query("$query");
		$result = array();
		$x=0;
		while($row=mysql_fetch_assoc($rst)){
			$result[$x] = $row;
			$x++;
		}				
		return $result;
	}

	public function insertid($query) {
        if (!mysql_query ("$query")) {
            $err1 = mysql_errno();
            $err2 = mysql_error();
            echo ("<h4>$query  $err1 $err2</h4>");
            exit;
        }
        $rwId=mysql_insert_id();
        return $rwId;
    }

	public function singlecolumn ($mysql) {
        $x = 0;
        $result = mysql_query($mysql);
        $q = array() ;
        while ($row = mysql_fetch_array ($result) ) {
            $q[$x] = $row[0];
            $x++;
        }
        mysql_free_result ($result);
        return $q;
    }
	
	function runQuery($query) {
		$result = mysql_query($query);
		while($row=mysql_fetch_assoc($result)) {
			$resultset[] = $row;
		}		
		if(!empty($resultset))
			return $resultset;
	}

	public function Extract_Single($query) {
        $x = 0;
        $result = mysql_query($query);
        while ( $row = mysql_fetch_array ($result) ) {
            $q[$x] = $row[0];
            $implode[] = $q[$x] ;
            $x++;
        }
        mysql_free_result ($result);
        return @implode(',', $implode);
    }

	public function check1column($table,$column,$v1) {
        if (! $result=mysql_query ("select * from $table where $column ='$v1'")) {
            $men = mysql_errno();
            $mem = mysql_error();
            echo ("<h4>$mysql  $men $mem</h4>");
            exit();
        }
        $row=mysql_fetch_array ($result);
        mysql_free_result ($result);
        if ($row[0])
            $var =  1;
        else
            $var =  0;
        return $var;
    }
	
    public function check2column($table,$column1,$v1,$column2,$v2) {
        if (! $result=mysql_query ("select * from $table where $column2 ='$v2' and $column1='$v1'")) {
            $men = mysql_errno();
            $mem = mysql_error();
            echo ("<h4>$mysql  $men $mem</h4>");
            exit();
        }
        $row=mysql_fetch_array ($result);
        mysql_free_result ($result);
        if ($row[0])
            $var =  1;
        else
            $var =  0;
        return $var;
    }
	
    public function page_break($db_object,$return_content,$adjacents,$total_pages,$limit,$targetpage,$page) {
        if ($page == 0) $page = 1;
        $prev = $page - 1;
        $next = $page + 1;
        $lastpage = ceil($total_pages/$limit);
        $lpm1 = $lastpage - 1;
        $targetpage = $targetpage."page";
        $pagination = "";
        if($lastpage > 1) {
            $pagination .= "<div class=\"pagination\">";
            if ($page > 1)
                $pagination.= "<a href=\"$targetpage=$prev\">&laquo; previous</a>";
            else
                $pagination.= "<span class=\"disabled\">&laquo; previous</span>";
            if ($lastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"$targetpage=$counter\">$counter</a>";
                }
            }
            elseif($lastpage > 5 + ($adjacents * 2)) {
                if($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $pagination.= "<span class=\"current\">$counter</span>";
                        else
                            $pagination.= "<a href=\"$targetpage=$counter\">$counter</a>";
                    }
                    $pagination.= "...";
                    $pagination.= "<a href=\"$targetpage=$lpm1\">$lpm1</a>";
                    $pagination.= "<a href=\"$targetpage=$lastpage\">$lastpage</a>";
                }
                elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $pagination.= "<a href=\"$targetpage=1\">1</a>";
                    //$pagination.= "<a href=\"$targetpage=2\">2</a>";
                    $pagination.= "...";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $pagination.= "<span class=\"current\">$counter</span>";
                        else
                            $pagination.= "<a href=\"$targetpage=$counter\">$counter</a>";
                    }
                    $pagination.= "...";
                    $pagination.= "<a href=\"$targetpage=$lpm1\">$lpm1</a>";
                    $pagination.= "<a href=\"$targetpage=$lastpage\">$lastpage</a>";
                }
                //close to end; only hide early pages
                else {
                    $pagination.= "<a href=\"$targetpage=1\">1</a>";
                    $pagination.= "<a href=\"$targetpage=2\">2</a>";
                    $pagination.= "...";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page)
                            $pagination.= "<span class=\"current\">$counter</span>";
                        else
                            $pagination.= "<a href=\"$targetpage=$counter\">$counter</a>";
                    }
                }
            }
            if ($page < $counter - 1)
                $pagination.= "<a href=\"$targetpage=$next\">next &raquo;</a>";
            else
                $pagination.= "<span class=\"disabled\">next &raquo;</span>";
            $pagination.= "</div>\n";
        }
        $return_content=str_replace ( "<{pagination}>", $pagination, $return_content);
        return $return_content;
    }
	
	public function counting_days(){
		$start = '2015-01-01';
		$end = date("Y/m/d");
		$diff = (strtotime($end)- strtotime($start))/24/3600; 
		
		return $diff;
	}
	
	public function drop_down($array,$getval,$getname){
		$list = "";
		for($astrn=1; $astrn<count($array); $astrn++){
			if($astrn == $getval)
				$list .= "<input type='radio' id='$getname' name='$getname' value='$astrn' checked>$array[$astrn] &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			else	
				$list .= " <input type='radio' id='$getname' name='$getname' value='$astrn'>$array[$astrn]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		return $list;
	}
	
	public function drop_down_view($array,$getval){
		$list = "";
		for($astrn=1; $astrn<count($array); $astrn++){
			if($astrn == $getval)
				$list .= $array[$astrn];
		}
		return $list;
	}
	
	public function drop_down_mail($array,$getval){
		$list = $array[$getval];
		return $list;
	}
	
	public function first_letter($string){
		$expr = '/(?<=\s|^)[a-z]/i';
		preg_match_all($expr, $string, $matches);
		$result = implode('', $matches[0]);
		$result = strtoupper($result);
		return $result;
	}	
	
	public function int_val($string){
		$ret = preg_replace("/[^0-9]/","",$string);
		return $ret;
	}
	
	public function hidecontrols($string){
		if($string == "Admin")
			$ret = "";
		else
			$ret = "<style>.btn-default{display:none;} .cntrhid{display:none;}</style>";
		
		return $ret;
	}
	
	// New Functions For Tamarai MLM Customization (Sakthi M)
	public function PlacementID($spnsrs) {
		$spnsrid=explode(",",$spnsrs);
		for($i=0;$i<count($spnsrid);$i++) {
			$sid=$spnsrid[$i];
			$spsrcnt=self::singlerec("select count(*) as tot from mlm_register where user_placementid='$sid'");
			$spsrcnt=$spsrcnt['tot'];
			if($spsrcnt<2) { $this->placement=$sid; break; }
			if(($i+1)==count($spnsrid)) {
				$spnsr=self::Extract_Single("select user_profileid from mlm_register where FIND_IN_SET(user_sponserid, '$spnsrs') and user_status='0' order by user_id asc");
				if(!empty($spnsr)) self::PlacementID($spnsr);
			}
		}
	}
	
	public function userName($profileid) {
		$userInfo=self::singlerec("select user_fname,user_lname from mlm_register where user_profileid='$profileid' and user_status='0'");
		if(!empty($userInfo['user_fname'])) 
			return $userInfo['user_fname']." ".$userInfo['user_lname'];
		else return "";
	}
	
	public function usrMemAmt($profileid) {
		$uInfo=self::singlerec("select mem_package from mlm_register where user_profileid='$profileid'");
		$memInfo=self::singlerec("select act_amount from mlm_membership where id='$uInfo[mem_package]'");
		return $memInfo['act_amount'];
	}
	
	public function refBonus($profileid, $paidAmt) {
		$uInfo=self::singlerec("select user_sponserid,mem_package from mlm_register where user_profileid='$profileid'");
		$sponserid=$uInfo['user_sponserid'];
		$chkref=self::singlerec("select id from mlm_payout where user_id='$sponserid' and from_id='$profileid' and bonus_type='0'");
		if($chkref['id']=="") {
			$cmInfo=self::singlerec("select * from mlm_basic_comission");
			$cmType=$cmInfo['ref_bonustype'];
			$cmAmt=$cmInfo['ref_bonusamt'];
			if($cmType==1) $amt=$cmAmt;
			else $amt=($cmAmt/100)*$paidAmt; $amt=floor($amt);
			$time=time();
			$set="user_id='$sponserid'";
			$set.=",from_id='$profileid'";
			$set.=",amount='$amt'";
			$set.=",reason='Referal Bonus'";
			$set.=",bonus_type='0'";
			$set.=",date='$time'";
			self::insertrec("insert into mlm_payout set $set");
			$i=mysql_insert_id();
			return $i;
		}
	}
	
	public function completedPair($pid) {
		$paircount=isset($paircount)?$paircount:'0';
		$pid=explode(",", $pid);
		foreach($pid as $profileid) {
			$spnsrcnt=self::singlerec("select count(*) as tot from mlm_register where user_placementid='$profileid'");
			if($spnsrcnt['tot']>=2) $paircount++;
		}
		$spnsrs=self::Extract_Single("select user_profileid from mlm_register where FIND_IN_SET(user_placementid,'$pid')");
		if(!empty($spnsrs)) self::completedPair($spnsrs);
		return $paircount;
	}
	
	public function pairCommission($profileid) {
		$paircount=self::completedPair($profileid);
		$pairAmt=self::singlerec("select * from mlm_pair_comission where pair_to='$paircount'");
		if(!empty($pairAmt)) {
			$percentage=$pairAmt['pair_amt'];
			$usrMemAmt=self::usrMemAmt($profileid);
			$comAmt=($percentage/100)*$usrMemAmt;
			$comAmt=floor($comAmt);
			if(!empty($comAmt)) {
				$time=time();
				$set="user_id='$profileid'";
				$set.=",amount='$comAmt'";
				$set.=",reason='Pair Bonus #$paircount'";
				$set.=",bonus_type='1'";
				$set.=",date='$time'";
				self::insertrec("insert into mlm_payout set $set");
			}
		}
	}
	
	public function productDiscountper($purchaseCount) {
		$disPer=self::singlerec("select dis_amt from mlm_discount_plan where $purchaseCount between dis_from and dis_to");
		return (int)$disPer['dis_amt'];
	}
	
	public function productDiscount($profileid, $purchaseCount) {
		$disPer=self::singlerec("select dis_amt from mlm_discount_plan where $purchaseCount between dis_from and dis_to");
		if(!empty($disPer)) {
			$usrMemAmt=self::usrMemAmt($profileid);
			$disAmt=($disPer['dis_amt']/100)*$usrMemAmt;
		}
		else $disAmt=0;
		return floor($disAmt);
	}
	
	public function firstUsr() {
		$uInfo=self::singlerec("select user_profileid from mlm_register order by user_id asc");
		return $uInfo['user_profileid'];
	}
	
	public function totalBal($profileid){
		$getBal=self::singlerec("select sum(amount) AS total from mlm_payout where user_id='$profileid' and status='1'");
		$result=$getBal['total'];		
		return floor($result);
	}
	
	public function withdrawBal($profileid){
		$getBal=self::singlerec("select sum(req_cvamount) AS total from mlm_withdrawrequsets where req_profileid='$profileid' and req_rpstatus='1'");
		$result=$getBal['total'];
		return floor($result);
	}
	
	public function availBal($tot,$with){
		$sum=$tot-$with;
		return floor($sum);
	}
	
	public function blankSpace($usrprofileid){
		$pid=explode(",",$usrprofileid);
		foreach($pid as $profileid) {
			$placement=self::singlerec("select count(*) as tot from mlm_register where user_placementid='$profileid'");
			$placement=(int)$placement['tot'];
			if($placement<2) $_SESSION['blankspc'][]=$profileid;
		}
		$nxtusers=self::Extract_Single("select user_profileid from mlm_register where FIND_IN_SET(user_placementid, '$usrprofileid')");
		if(!empty($nxtusers)) self::blankSpace($nxtusers);
	}
	
	public function commonMail($to,$subject,$msg){
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From:'.$website_name.'<'.$website_admin.'>' . "\r\n";
		if(!empty($to) && !empty($subject) && !empty($msg) && !empty($headers)){
			mail($to,$subject,$msg,$headers);
			return true;
		}else{
			return false;
		}
	}
	
}

$GT_vadmin=1;
while(list($key,$value)=@each($_POST)) {
	$$key=$value;
}
while(list($key,$value)=@each($_GET)) {
    $$key=$value;
}	
?>