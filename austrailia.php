<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Global It  Management</title>
   <link rel="icon" href="images/logo.png" type="image/png" sizes="16x16">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link href="css/bootstrap-4-navbar.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <!-- Static navbar Start-->
  <?php 
			require('header.php');
			?>
  <!-- SLIDER END  -->



<!-- members section start -->
<section class="Members">
  <div class="container">
    <h1 class="text-center">Members</h1> <br> <br>
    <div class="row">
      <div class="col">
        <div class="card-deck">
        <div class="card">
        <img class="card-img-top img-fluid" src="images/card1.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Md. Towhidul Islam</h5>
          <p class="card-text">
            <span>MEMBER</span> <br>
            <span>ALGERIAN</span> <br> <br>

            <span><a href="#"><i class="fab fa-facebook"></i></a></span>
            <span><a href="#"><i class="fab fa-twitter"></i></a></span>
            <span><a href="#"><i class="fab fa-twitch"></i></a></span>
          </p>
          <p class="card-text"><small class="text-muted">joined 3 months ago</small></p>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top img-fluid" src="images/card1.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Md. Nadim</h5>
          <p class="card-text">
            <span>MEMBER</span> <br>
            <span>ALGERIAN</span> <br> <br>
            <span><a href="#"><i class="fab fa-facebook"></i></a></span>
            <span><a href="#"><i class="fab fa-twitter"></i></a></span>
            <span><a href="#"><i class="fab fa-twitch"></i></a></span>
          </p>      <p class="card-text"><small class="text-muted">joined 3 months ago</small></p>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top img-fluid" src="images/card1.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Md. Shohan</h5>
          <p class="card-text">
            <span>MEMBER</span> <br>
            <span>ALGERIAN</span> <br> <br>

            <span><a href="#"><i class="fab fa-facebook"></i></a></span>
            <span><a href="#"><i class="fab fa-twitter"></i></a></span>
            <span><a href="#"><i class="fab fa-twitch"></i></a></span>
          </p>
          <p class="card-text"><small class="text-muted">joined  3 months ago</small></p>
        </div>
      </div>
    </div>
      </div>
    </div>

<br>
<div class="row">
  <div class="col">
    <div class="card-deck">
    <div class="card">
    <img class="card-img-top img-fluid" src="images/card1.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Md. Towhidul Islam</h5>
      <p class="card-text">
        <span>MEMBER</span> <br>
        <span>ALGERIAN</span> <br> <br>

        <span><a href="#"><i class="fab fa-facebook"></i></a></span>
        <span><a href="#"><i class="fab fa-twitter"></i></a></span>
        <span><a href="#"><i class="fab fa-twitch"></i></a></span>
      </p>
      <p class="card-text"><small class="text-muted">joined 3 months ago</small></p>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top img-fluid" src="images/card1.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Md. Nadim</h5>
      <p class="card-text">
        <span>MEMBER</span> <br>
        <span>ALGERIAN</span> <br> <br>

        <span><a href="#"><i class="fab fa-facebook"></i></a></span>
        <span><a href="#"><i class="fab fa-twitter"></i></a></span>
        <span><a href="#"><i class="fab fa-twitch"></i></a></span>
      </p>      <p class="card-text"><small class="text-muted">joined 3 months ago</small></p>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top img-fluid" src="images/card1.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Md. Shohan</h5>
      <p class="card-text">
        <span>MEMBER</span> <br>
        <span>ALGERIAN</span> <br> <br>

        <span><a href="#"><i class="fab fa-facebook"></i></a></span>
        <span><a href="#"><i class="fab fa-twitter"></i></a></span>
        <span><a href="#"><i class="fab fa-twitch"></i></a></span>
      </p>
      <p class="card-text"><small class="text-muted">joined  3 months ago</small></p>
    </div>
  </div>
</div>
  </div>
</div>
  </div>
</section>
<!-- members section ends -->



  <!-- footer start -->
  <?php 
			require('footer.php');
			?>
  <!-- footer end -->






  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  <script src="js/bootstrap-4-navbar.js"></script>
</body>
</html>
