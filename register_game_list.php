<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Global It  Management</title>
   <link rel="icon" href="images/logo.png" type="image/png" sizes="16x16">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link href="css/bootstrap-4-navbar.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <!-- Static navbar Start-->
  <?php 
			require('header.php');
			?>
  <!-- SLIDER END  -->



<!-- members section start -->
<section class="Members">
  <div class="container">
  <div class="row">
          <div class="col-md-4">
            <div class="text-center">
              <img class= "img-fluid img-thumbnail mt-2" src="images/dota2.jpg" alt="news" style="height: 300px; width: 350px"><h3><a href="dota_2_terms_condition.php" style="color:#fff;text-decoration:none">Dota 2</a></h3> <br>
              <img class= "img-fluid img-thumbnail mt-2" src="images/csgo.jpg" alt="news"style="height: 300px; width: 350px"><h3><a href="csgo_terms_condition.php" style="color:#fff;text-decoration:none">Counter Strike: GO</a> </h3>
            </div>
          </div>

          <div class="col-md-4">
            <div class="text-center">
              <img class= "img-fluid img-thumbnail mt-2" src="images/RainbowSixSiegelogo.jpg" alt="news" style="height: 300px; width: 350px"><h3><a href="RainbowSixSiege_terms_condition.php" style="color:#fff;text-decoration:none">Rainbow Six | Siege</a></h3><br>           
              <img class= "img-fluid img-thumbnail mt-2" src="images/FIFA18.jpg" alt="news"style="height: 300px; width: 350px"><h3><a href="FIFA18_terms_condition.php" style="color:#fff;text-decoration:none">FIFA 18</a> </h3>
            </div>
          </div>


          <div class="col-md-4">
            <div class="text-center">
              <img class= "img-fluid img-thumbnail mt-2" src="images/LeagueOfLegends.png" alt="news"style="height: 300px; width: 350px"><h3><a href="LeagueOfLegends_terms_condition.php" style="color:#fff;text-decoration:none">League of Legends</a> </h3><br>
              
            </div>
          </div>
        </div>

  </div>
</section>
<!-- members section ends -->



  <!-- footer start -->
  <?php 
			require('footer.php');
			?>
  <!-- footer end -->






  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  <script src="js/bootstrap-4-navbar.js"></script>
</body>
</html>
