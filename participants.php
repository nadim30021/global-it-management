<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Global It  Management</title>
  <link rel="icon" href="images/logo.png" type="image/png" sizes="16x16">
  <link rel="stylesheet" href="css/style.css">

  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link href="css/bootstrap-4-navbar.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>

<body>
  <!-- Static navbar Start-->
   <?php
			require('header.php');
			?>
  <!-- SLIDER END  -->


<!-- Participants Start  -->

<?php 
$gid =$_REQUEST['id'];

$qry = "select * from team_reg_information where gameid='$gid'";
       $res = mysql_query($qry);
while($row=mysql_fetch_array($res))
  { 
    ?>
<section class="Participants p-5 ">
  <div class="container">
    <div class="text-center">
      <img class="TeamLogo img-fluid" src="images/<?php echo $row['team_logo'];?>" alt="teamlogo" style="height:300px ; width:300px">
      <h3 class="text-center"> <?php echo $row['team_name'];?> </h3>
    </div>
    <br>
    <div class="row">
      <div class="col-md-3 text-center" style="padding-bottom:30px">
        <h4><?php echo $row['captin_name'];?></h4>
        <img class="img-fluid img-thumbnail" src="uploads/team_member_photo/<?php echo $row['captin_img'];?>" alt="participants" style="height:200px ; width:300px;">
        <h4><?php echo $row['captin_game_name'];?></h4>
      </div>

      <div class="col-md-3 text-center" style="padding-bottom:30px">
        <h4><?php echo $row['player2_name'];?></h4>
        <img class="img-fluid img-thumbnail" src="uploads/team_member_photo/<?php echo $row['player2_img'];?>" alt="participants" style="height:200px ; width:300px;">
        <h4><?php echo $row['player2_game_name'];?></h4>
      </div>

      <div class="col-md-3 text-center" style="padding-bottom:30px">
        <h4><?php echo $row['player3_name'];?></h4>
        <img class="img-fluid img-thumbnail" src="uploads/team_member_photo/<?php echo $row['player3_img'];?>" alt="participants" style="height:200px ; width:300px;">
        <h4><?php echo $row['player3_game_name'];?></h4>
      </div>
      <div class="col-md-3 text-center" style="padding-bottom:30px">
        <h4><?php echo $row['player4_name'];?></h4>
        <img class="img-fluid img-thumbnail" src="uploads/team_member_photo/<?php echo $row['player4_img'];?>" alt="participants" style="height:200px ; width:300px;">
        <h4><?php echo $row['player4_game_name'];?></h4>
      </div>
      <div class="col-md-3 text-center" style="padding-bottom:30px">
        <h4><?php echo $row['player5_name'];?></h4>
        <img class="img-fluid img-thumbnail" src="uploads/team_member_photo/<?php echo $row['player5_img'];?>" alt="participants" style="height:200px ; width:300px;">
        <h4><?php echo $row['player5_game_name'];?></h4>
      </div>
      <div class="col-md-3 text-center" style="padding-bottom:30px">
        <h4><?php echo $row['sub1_name'];?></h4>
        <img class="img-fluid img-thumbnail" src="uploads/team_member_photo/<?php echo $row['sub1_img'];?>" alt="participants" style="height:200px ; width:300px;">
        <h4><?php echo $row['sub1_game_name'];?></h4>
      </div>
      <div class="col-md-3 text-center" style="padding-bottom:30px">
        <h4><?php echo $row['sub2_name'];?></h4>
        <img class="img-fluid img-thumbnail" src="uploads/team_member_photo/<?php echo $row['sub2_img'];?>" alt="participants" style="height:200px ; width:300px;">
        <h4><?php echo $row['sub2_game_name'];?></h4>
      </div>
    </div>
  </div>
</section>
    

 <?php }

?>










<!-- Participants End  -->




<!-- footer start -->
 <?php
			require('footer.php');
			?>
<!-- footer end -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

  <script src="js/bootstrap-4-navbar.js"></script>
</body>

</html>
